import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LinkService {

  constructor() { }

  getLinks(){
    return [
      {
        label: 'Home',
        path: 'home'
      },
      {
        label: 'Portafolio',
        path: 'portafolio'
      },
      {
        label: 'Contacto',
        path: 'contacto'
      }
    ]
  }


}
