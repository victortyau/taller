import { Component, OnInit } from '@angular/core';
import { Link } from 'src/app/models/link';
import { LinkService } from 'src/app/services/Link/link.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  links: Link[];

  constructor(private linkService: LinkService) { }

  ngOnInit() {
    this.links = this.linkService.getLinks();
  }

}
